/**
 * Created by kuldeep on 11/12/16.
 */
var contactModule = angular.module('contact');

contactModule.controller('contactCtrl', function ($scope, $http, $httpParamSerializerJQLike) {

    $scope.greet = "Welcome to Shri Krishana Travels!!";

    /**
     * Send a contact request.
     */
    $scope.send = function () {

        validate();
        if ($scope.invalidName || $scope.invalidPhoneEmail) return;


        var url = 'https://api.tarkshala.com/drop/';
        $http({
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            url: url,
            data: {
                "name": $scope.name,
                "phone": $scope.phone,
                "email": $scope.email,
                "message": $scope.message,
                "tags": {
                    "source" : "shrikrishanatravels.com"
                }
            }
        }).then(function (response) {
            console.log(response.data);
            $scope.submitSuccess = true;
            flushContactDetails();
        }, function (error) {
            console.log(error);
            $scope.submitSuccess = false;
            handleDropContactError(error);
            flushContactDetails();
        });
    };

    /**
     * Validate form
     */
    var validate = function () {

        $scope.invalidName = false;
        $scope.invalidPhoneEmail = false;
        if (!$scope.name || $scope.name.length == 0) {
            $scope.invalidName |= true;
        }
        if ((!$scope.phone || $scope.phone.length == 0) && (!$scope.email || $scope.email.length == 0)) {
            $scope.invalidPhoneEmail |= true;
        }
    }

    /**
     * Handle error while dropping contact.
     *
     * @param error response
     */
    var handleDropContactError = function (error) {
        // todo: replace with a popup
//        alert("Your details couldn't be saved. " +
//            "Please call 8569874941 or drop an email at travel.shrikrishana@gmail.com " +
//            "We regret for the inconvenience caused ")
    }

    /**
     * Flush contact details from local context.
     */
    var flushContactDetails = function () {
        $scope.name = "";
        $scope.email = "";
        $scope.phone = "";
        $scope.message = "";
    }

});